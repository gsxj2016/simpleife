cmake_minimum_required(VERSION 3.14)
project(SimpleIFE)

set(CMAKE_CXX_STANDARD 14)

set(EXEC_NAME SimpleIFE)
add_executable(${EXEC_NAME}
        src/main.cpp
        src/maxflow.cpp src/maxflow.hpp
        src/app.cpp src/app.hpp
        )

# Library configuration
if (MSVC)
    set(SFML_ROOT third_party/SFML-2.5.1-MSVC64)
elseif (MINGW)
    set(SFML_ROOT third_party/SFML-2.5.1-MinGW64)
endif ()

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})
find_package(OpenGL REQUIRED)
find_package(SFML 2 REQUIRED system window graphics)

include_directories(${SFML_INCLUDE_DIR})
target_link_libraries(${EXEC_NAME} ${OPENGL_LIBRARIES} ${SFML_LIBRARIES})

# Compile flag configuration
if (CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wnarrowing -Wconversion")
    set(CMAKE_CXX_FLAGS_RELEASE  "${CMAKE_CXX_FLAGS_RELEASE} -march=native")
endif ()

if (MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /utf-8")
endif ()

message(STATUS "CXX FLAGS         :${CMAKE_CXX_FLAGS}")
message(STATUS "CXX FLAGS(DEBUG)  :${CMAKE_CXX_FLAGS_DEBUG}")
message(STATUS "CXX FLAGS(RELEASE):${CMAKE_CXX_FLAGS_RELEASE}")

