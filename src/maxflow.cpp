#include "maxflow.hpp"
#include <queue>
#include <cstdlib>

namespace SimpleIFE {

    MaxFlow::MaxFlow(int maxNode, int maxEdge)
            : n(0), M(2), xn(maxNode), xM(maxEdge + 2),
              lvl(std::make_unique<int[]>(maxNode)),
              g(std::make_unique<int[]>(maxNode)),
              sg(std::make_unique<int[]>(maxNode)),
              e(std::make_unique<Edge[]>(maxEdge)) {}

    int MaxFlow::newNode() {
        int a = n++;
        if (a >= xn)std::abort();
        g[n] = 0;
        lvl[n] = -1;
        return a;
    }

    int MaxFlow::newEdge(int a, int b, int64_t c1, int64_t cr) {
        int i1 = M++;
        int ir = M++;
        if (ir >= xM)std::abort();
        if (i1 != (ir ^ 1))std::abort();
        e[i1].cap = e[i1].init = c1, e[i1].to = b, e[i1].next = g[a], g[a] = i1;
        e[ir].cap = e[ir].init = cr, e[ir].to = a, e[ir].next = g[b], g[b] = ir;
        return i1;
    }

    int64_t MaxFlow::push(int a, int64_t fl, int sink) {
        int64_t pd = 0;
        if (a == sink) return fl;
        if (fl == 0) return 0;
        for (int ii = sg[a]; ii; ii = e[ii].next) {
            int b = e[ii].to;
            if (lvl[b] != lvl[a] + 1)continue;
            int64_t ff = push(b, std::min(e[ii].cap, fl), sink);
            fl -= ff, pd += ff, e[ii].cap -= ff, e[ii ^ 1].cap += ff;
            if (!fl)break;
            sg[a] = e[ii].next;
        }
        return pd;
    }

    bool MaxFlow::clv(int src, int sink) {
        for (int i = 0; i < n; ++i)lvl[i] = -1, sg[i] = g[i];
        std::queue<int> Q;
        Q.push(src), lvl[src] = 1;
        while (!Q.empty()) {
            int a = Q.front();
            Q.pop();
            for (int ii = g[a]; ii; ii = e[ii].next) {
                if (!e[ii].cap)continue;
                int b = e[ii].to;
                if (~lvl[b])continue;
                lvl[b] = lvl[a] + 1;
                Q.push(b);
            }
        }
        return ~lvl[sink];
    }

    int64_t MaxFlow::flow(int src, int sink, int64_t fl) {
        int64_t pd = 0;
        while (fl && clv(src, sink)) {
            int64_t ff = push(src, fl, sink);
            pd += ff, fl -= ff;
        }
        return pd;
    }

}
