#include <iostream>
#include "app.hpp"

int main(int argc, char **argv) {
    static SimpleIFE::App app;
    if (argc == 2 && app.loadImage(argv[1])) {
        app.run();
    }
}