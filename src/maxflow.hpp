#ifndef SIMPLEIFE_MAXFLOW_HPP
#define SIMPLEIFE_MAXFLOW_HPP

#include <cstdint>
#include <vector>
#include <memory>

namespace SimpleIFE {

    struct Edge {
        int64_t cap;
        int64_t init;
        int to;
        int next;
    };

    struct MaxFlow {
        int n, M, xn, xM;
        std::unique_ptr<int[]> lvl;
        std::unique_ptr<int[]> g;
        std::unique_ptr<int[]> sg;
        std::unique_ptr<Edge[]> e;

        MaxFlow(int maxNode, int maxEdge);

        int newNode();

        int newEdge(int a, int b, int64_t c1, int64_t cr);

        int64_t push(int a, int64_t fl, int sink);

        bool clv(int src, int sink);

        int64_t flow(int src, int sink, int64_t fl);

    };

}


#endif //SIMPLEIFE_MAXFLOW_HPP
