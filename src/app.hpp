#ifndef SIMPLEIFE_APP_HPP
#define SIMPLEIFE_APP_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include "maxflow.hpp"

namespace SimpleIFE {
    const int64_t FORBID_WEIGHT = 999999999999LL;
    const int64_t N_WEIGHT_MAX = 1000LL;
    const double N_EXP_FACTOR = 0.1;

    struct App {
        sf::Image image;
        sf::Image selectMask;
        sf::Image imageMask;
        sf::Texture imgTex;
        sf::Texture selTex;
        sf::Texture mTex;
        std::unique_ptr<MaxFlow> flow;
        std::unique_ptr<int[]> fPixel;
        std::unique_ptr<int[]> sPixel;
        std::unique_ptr<int[]> tPixel;
        std::unique_ptr<int[]> pixLabel;
        int iWidth, iHeight;
        int fSrc, fSink;

        bool loadImage(const char *fn);

        void run();

        static int64_t colorDiff(const sf::Color &a, const sf::Color &b);

        void setPredefinedSingle(int x, int y, bool bg);

        void doCut();

    };
}

#endif //SIMPLEIFE_APP_HPP
