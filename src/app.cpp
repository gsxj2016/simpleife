#include "app.hpp"
#include <cmath>

namespace SimpleIFE {

    bool App::loadImage(const char *fn) {
        bool success = image.loadFromFile(fn);
        if (success) {
            iWidth = image.getSize().x;
            iHeight = image.getSize().y;
            int sz = iWidth * iHeight;
            fPixel = std::make_unique<int[]>(sz);
            sPixel = std::make_unique<int[]>(sz);
            tPixel = std::make_unique<int[]>(sz);
            pixLabel = std::make_unique<int[]>(sz);
            flow = std::make_unique<MaxFlow>(sz * 3, sz * 10);
            fSrc = flow->newNode();
            fSink = flow->newNode();
            for (int y = 0; y < iHeight; ++y) {
                int offset = y * iWidth;
                for (int x = 0; x < iWidth; ++x) {
                    int vid;
                    sf::Color c1 = image.getPixel(x, y);
                    fPixel[offset + x] = vid = flow->newNode();
                    sPixel[offset + x] = flow->newEdge(fSrc, vid, 0, 0);
                    tPixel[offset + x] = flow->newEdge(vid, fSink, 0, 0);
                    pixLabel[offset + x] = 0;
                    if (y > 0) {
                        int kr = fPixel[offset - iWidth + x];
                        sf::Color c2 = image.getPixel(x, y - 1);
                        int64_t wd = colorDiff(c1, c2);
                        flow->newEdge(vid, kr, wd, wd);
                    }
                    if (x > 0) {
                        int kr = fPixel[offset + x - 1];
                        sf::Color c2 = image.getPixel(x - 1, y);
                        int64_t wd = colorDiff(c1, c2);
                        flow->newEdge(vid, kr, wd, wd);
                    }
                }
            }
        }
        return success;
    }

    void App::run() {
        sf::RenderWindow window(sf::VideoMode(iWidth, iHeight),
                                "Simple FG Extraction",
                                sf::Style::Titlebar | sf::Style::Close);
        window.setKeyRepeatEnabled(false);
        window.setFramerateLimit(30);
        imgTex.loadFromImage(image);
        sf::Sprite sprite(imgTex);
        selectMask.create(iWidth, iHeight, sf::Color::Transparent);
        imageMask.create(iWidth, iHeight, sf::Color::Transparent);
        selTex.loadFromImage(selectMask);
        mTex.loadFromImage(imageMask);
        sf::Sprite sel(selTex);
        sf::Sprite mask(mTex);
        int setPt = 0;
        bool showMask = true;

        while (window.isOpen()) {
            sf::Event event;
            while (window.pollEvent(event)) {
                if (event.type == sf::Event::Closed) {
                    window.close();
                } else if (event.type == sf::Event::MouseButtonPressed) {
                    if (!setPt) {
                        showMask = false;
                        window.setFramerateLimit(0);
                        sf::Mouse::Button btn = event.mouseButton.button;
                        setPt = btn == sf::Mouse::Left ? 1 : 2;
                        int x = event.mouseButton.x;
                        int y = event.mouseButton.y;
                        setPredefinedSingle(x, y, setPt == 2);
                    }
                } else if (event.type == sf::Event::MouseButtonReleased) {
                    window.setFramerateLimit(60);
                    setPt = 0;
                    showMask = true;
                    selTex.loadFromImage(selectMask);
                    doCut();
                } else if (event.type == sf::Event::MouseMoved) {
                    if (setPt) {
                        setPredefinedSingle(event.mouseMove.x,
                                            event.mouseMove.y,
                                            setPt == 2);
                        selTex.loadFromImage(selectMask);
                    }
                } else if (event.type == sf::Event::KeyReleased) {
                    if (event.key.code == sf::Keyboard::Space) {
                        showMask = !showMask;
                    }
                }
            }
            window.clear();
            window.draw(sprite);
            if (showMask)window.draw(mask);
            window.draw(sel);
            window.display();
        }
    }

    int64_t App::colorDiff(const sf::Color &a, const sf::Color &b) {
        double rd = (a.r - b.r) / 255.0;
        double gd = (a.g - b.g) / 255.0;
        double bd = (a.b - b.b) / 255.0;
        return static_cast<int64_t>(N_WEIGHT_MAX * exp(-(rd * rd + gd * gd + bd * bd) / N_EXP_FACTOR));
    }

    void App::doCut() {
        clock_t t1, t2;
        printf("Processing graph cut... Please wait.\n");
        t1 = std::clock();
        imageMask.create(iWidth, iHeight, sf::Color::Transparent);
        int64_t energy = flow->flow(fSrc, fSink, INT64_MAX / 2);
        std::printf("Energy change: %lld\n", energy);
        sf::Color mc;
        for (int y = 0; y < iHeight; ++y) {
            int offset = y * iWidth;
            for (int x = 0; x < iWidth; ++x) {
                int maskBlack = ((y >> 3) & 1) ^((x >> 3) & 1);
                mc.r = mc.g = mc.b = 128;
                if (!maskBlack)mc = sf::Color::White;
                int idx = fPixel[offset + x];
                if (!~(flow->lvl[idx])) {
                    mc.a = 200;
                } else {
                    mc.a = 0;
                }
                imageMask.setPixel(x, y, mc);
            }
        }
        mTex.loadFromImage(imageMask);
        t2 = std::clock();
        printf("Time used: %.3f s\n", static_cast<double>(t2 - t1) / CLOCKS_PER_SEC);
    }

    void App::setPredefinedSingle(int x, int y, bool bg) {
        if (x < 0 || y < 0 || x >= iWidth || y >= iHeight)return;
        int offset = y * iWidth;
        if (pixLabel[offset + x] != 0) return;
        pixLabel[offset + x] = bg ? 2 : 1;
        int idx = bg ? tPixel[offset + x] : sPixel[offset + x];
        flow->e[idx].cap += FORBID_WEIGHT;
        sf::Color mc = bg ? sf::Color::Red : sf::Color::Green;
        mc.a = 127;
        selectMask.setPixel(x, y, mc);
    }
}
